package org.grease.proveance.gui.controller;


import org.grease.proveance.gui.view.mainwindow.information.InformationView;
import org.grease.proveance.gui.view.mainwindow.information.graphelements.NodeView;
import org.grease.proveance.gui.view.mainwindow.information.graphelements.NodeViewReductions;
import org.grease.proveance.gui.view.mainwindow.information.graphelements.RelationshipView;
import org.grease.provenance.model.ProvenanceModel;
import org.grease.provenance.model.graph.ProvenanceDependency;
import org.grease.provenance.model.graph.ProvenanceGraph;
import org.grease.provenance.model.graph.ProvenanceNode;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Marcus on 21.02.2018.
 */

public class InformationViewController {
    private final MainProvenanceViewCotroller controller;
    private final ProvenanceModel provenanceModel;

    public InformationViewController(ProvenanceModel provenanceModel, MainProvenanceViewCotroller controller) {
        this.controller = controller;
        this.provenanceModel = provenanceModel;
    }

    //Removes panel, that shows information about nodes/dependencies
    private void clearInformationView(InformationView informationView) {
        NodeView nodeView = informationView.getNodeView();
        RelationshipView relationshipView = informationView.getRelationshipView();
        nodeView.setVisible(false);
        relationshipView.setVisible(false);
    }

    //----------------------------Node View:----------------------------------------------------------------------------

    //loads information about chosen provenance node and displays it in the Informationview
    public void updateNodeInformationView(InformationView informationView, ProvenanceNode provenanceNode) {
        clearInformationView(informationView);
        NodeView nodeView = informationView.getNodeView();
        clearNodeView(nodeView);
        fillNodeViewInformation(nodeView, provenanceNode);
        informationView.add(nodeView, BorderLayout.WEST);
        nodeView.setVisible(true);

    }

    private void clearNodeView(NodeView nodeView) {

        nodeView.getIdTextfield().setText("");
        nodeView.getRelationshipTextField().setText("");
        nodeView.getTypeTextField().setText("");
        nodeView.getDescriptionTextArea().setText("");
        nodeView.getNodeViewReductionsPanel().removeAll();
    }

    private void fillNodeViewInformation(NodeView nodeView, ProvenanceNode provenanceNode) {
        nodeView.getIdTextfield().setText(provenanceNode.getId());
        nodeView.getDescriptionTextArea().setText(provenanceNode.getDescription());
        nodeView.getIdTextfield().setText(provenanceNode.getId());
        nodeView.getTypeTextField().setText(provenanceNode.getProvenanceNodeType().toString());
        this.fillNodeViewReductions(nodeView.getNodeViewReductionsPanel(), provenanceNode);
    }

    //    private void fillNodeViewReductions(NodeViewReductions nodeViewReductions, ProvenanceNode provenanceNode) {
//        nodeViewReductions.setLayout(new GridBagLayout());
//        Font serif = new Font("Arial", Font.BOLD, 18);
//
//        HashMap<String, String> possibleReductions = provenanceNode.getReduceMap();
//        int num = 0;
//        for (String key : possibleReductions.keySet()) {
//            nodeViewReductions.setPreferredSize(new Dimension(360, 40 + (num + 1) * 35));
//            JTextField keyTextField = new JTextField(key);
//            keyTextField.setFont(serif);
//            JTextField valueTextField = new JTextField(possibleReductions.get(key));
//            valueTextField.setFont(serif);
//            JLabel reductionIdLabel = new JLabel("Reduction id:");
//            reductionIdLabel.setFont(serif);
//            JLabel clusterLabel = new JLabel("Cluster id: ");
//            clusterLabel.setFont(serif);
//            nodeViewReductions.add(reductionIdLabel, fillElementIntoGridLayout(0, num));
//            nodeViewReductions.add(keyTextField, fillElementIntoGridLayout(1, num));
//            nodeViewReductions.add(clusterLabel, fillElementIntoGridLayout(2, num));
//            nodeViewReductions.add(valueTextField, fillElementIntoGridLayout(3, num));
//            num++;
//        }
//
//        JTextField keyTextField = new JTextField("");
//        JTextField valueTextField = new JTextField("");
//
//        JLabel reductionIdLabel = new JLabel("Reduction id:");
//        reductionIdLabel.setFont(serif);
//        JLabel clusterLabel = new JLabel("Cluster id: ");
//        clusterLabel.setFont(serif);
//        keyTextField.setPreferredSize(new Dimension(70, 25));
//        keyTextField.setFont(serif);
//        valueTextField.setPreferredSize(new Dimension(70, 25));
//        valueTextField.setFont(serif);
//        nodeViewReductions.add(reductionIdLabel, fillElementIntoGridLayout(0, possibleReductions.keySet().size()));
//        nodeViewReductions.add(keyTextField, fillElementIntoGridLayout(1, possibleReductions.keySet().size()));
//        nodeViewReductions.add(clusterLabel, fillElementIntoGridLayout(2, possibleReductions.keySet().size()));
//        nodeViewReductions.add(valueTextField, fillElementIntoGridLayout(3, possibleReductions.keySet().size()));
//
//        keyTextField.addFocusListener(new FocusListener() {
//            @Override
//            public void focusGained(FocusEvent e) {
//
//            }
//
//            @Override
//            public void focusLost(FocusEvent e) {
//                if (!valueTextField.getText().isEmpty() && !keyTextField.getText().isEmpty()) {
//                    String reduction = keyTextField.getText();
//                    String cluster = valueTextField.getText();
//                    controller.getProvenanceEditorController()
//                              .getGeneratingController()
//                              .addReductionToNode(provenanceNode, reduction, cluster, "unnamed", "ENTITY");
//                }
//            }
//        });
//
//        valueTextField.addFocusListener(new FocusListener() {
//            @Override
//            public void focusGained(FocusEvent e) {
//
//            }
//
//            @Override
//            public void focusLost(FocusEvent e) {
//                if (!valueTextField.getText().isEmpty() && !keyTextField.getText().isEmpty()) {
//                    String reduction = keyTextField.getText();
//                    String cluster = valueTextField.getText();
//                    controller.getProvenanceEditorController()
//                              .getGeneratingController()
//                              .addReductionToNode(provenanceNode, reduction, cluster, "unnamed", "ENTITY");
//                }
//            }
//        });
//
//    }
    private void fillNodeViewReductions(NodeViewReductions nodeViewReductions, ProvenanceNode provenanceNode) {

        nodeViewReductions.setLayout(new GridBagLayout());
//        Font serif = new Font("Arial", Font.BOLD, 18);
        JTextArea textArea = new JTextArea();

        HashMap<String, String> possibleReductions = provenanceNode.getReduceMap();
        for (Map.Entry<String, String> e : possibleReductions.entrySet()) {
            textArea.append(String.format("%s -- %s \n", e.getKey(), e.getValue()));
        }
        nodeViewReductions.add(new JScrollPane(textArea));

    }

    private GridBagConstraints fillElementIntoGridLayout(int column, int row) {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridx = column;
        gridBagConstraints.gridy = row;

        return gridBagConstraints;
    }

    //----------------------------Edge View:----------------------------------------------------------------------------

    //loads information about chosen provenance dependency and displays it in the Informationview
    public void updateEdgeInformationView(InformationView informationView, ProvenanceDependency relationship) {
        clearInformationView(informationView);
        RelationshipView relationshipView = informationView.getRelationshipView();
        clearEdgeView(relationshipView);

        fillEdgeViewInformation(relationshipView, relationship);
        informationView.add(relationshipView, BorderLayout.WEST);
        relationshipView.setVisible(true);
    }

    private void fillEdgeViewInformation(RelationshipView relationshipView, ProvenanceDependency relationship) {

        relationshipView.setRelationshipID(relationship.getId());
        relationshipView.getTargetTextField().setText(relationship.getTargetProvenanceNode().getName());
        relationshipView.getSourceTextField().setText(relationship.getSourceProvenanceNode().getName());
        relationshipView.getIdTextfield().setText(relationship.getId());
        relationshipView.getDependencyTextField().setText(relationship.getProvenanceDependencyType().name());

        relationshipView.getRoleTextField().setText(relationship.getRole());
    }

    private void clearEdgeView(RelationshipView relationshipView) {
        relationshipView.getIdTextfield().setText("");
        relationshipView.getDependencyTextField().setText("");
        relationshipView.getSourceTextField().setText("");
        relationshipView.getTargetTextField().setText("");
    }

    //--------------------------Handler:--------------------------------------------------------------------------------

    //saves a nodes description from the text field to the provenance node
    public void handleDescriptionChanges() {
        NodeView nodeView = controller.provenanceEditorView.getInformationView().getNodeView();
        JTextArea descriptionTextArea = nodeView.getDescriptionTextArea();
        String jsonSpecification = descriptionTextArea.getText();
        ProvenanceGraph provenanceGraph = controller.getProvenanceEditorController()
                                                    .getProvenanceModel()
                                                    .getProvenanceGraph();
        String id = nodeView.getNodeId();
        ProvenanceNode provenanceNode = controller.getProvenanceEditorController()
                                                  .getProvenanceNodeById(provenanceGraph, id);
        provenanceNode.setDescription(jsonSpecification);

        controller.getProvenanceEditorController()
                  .getDatabaseController()
                  .addDescriptionToDatabase(id, jsonSpecification);
    }

    //saves a dependency's role from role text field
    public void handleRoleChanges() {
        RelationshipView relationshipView = controller.getMainView().getInformationView().getRelationshipView();
        JTextField roleTextField = relationshipView.getRoleTextField();
        String role = roleTextField.getText();

        ProvenanceGraph provenanceGraph = controller.getProvenanceEditorController()
                                                    .getProvenanceModel()
                                                    .getProvenanceGraph();
        String id = relationshipView.getRelationshipID();
        ProvenanceDependency provenanceDependency = controller.getProvenanceEditorController()
                                                              .getProvenanceRelationshipById(provenanceGraph, id);
        provenanceDependency.setRole(role);

        controller.getProvenanceEditorController().getDatabaseController().addRoleToDatabase(id, role);
    }

}
