package org.grease.proveance.gui.controller;

import org.grease.provenance.model.graph.ProvenanceGraph;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.swing.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class GUIFileManagementController {
    MainProvenanceViewCotroller mainProvenanceViewCotroller;

    private boolean debug = true;

    public GUIFileManagementController(MainProvenanceViewCotroller mainProvenanceViewCotroller) {
        this.mainProvenanceViewCotroller = mainProvenanceViewCotroller;
    }

    //Save graph in json format using provenance library
    public void saveGraphToFile() {
        ProvenanceGraph provenanceGraph = mainProvenanceViewCotroller.getProvenanceEditorController()
                                                                     .getProvenanceModel()
                                                                     .getProvenanceGraph();
        JFileChooser fileChooser = new JFileChooser();

        int returnVal = fileChooser.showOpenDialog(mainProvenanceViewCotroller.provenanceEditorView);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                File provenanceFile = new File(fileChooser.getSelectedFile().getPath());
                mainProvenanceViewCotroller.getProvenanceEditorController()
                                           .getFileManagementController()
                                           .saveGraph(provenanceGraph, provenanceFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //choose json file (created with provenance library) to load Graph
    public void loadGraphFromFile() {
        JFileChooser fileChooser = new JFileChooser();
        int returnVal = fileChooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                String path = fileChooser.getSelectedFile().getPath();
                loadProvenanceGraph(path);
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadProvenanceGraph(String path) throws IOException, ParseException {
        JSONObject graphObject = createJsonObject(path);
        mainProvenanceViewCotroller.createNewProvenanceModel();
        mainProvenanceViewCotroller.getProvenanceEditorController()
                                   .getFileManagementController()
                                   .loadGraphWithGui(graphObject, path);
        if (debug) {
            System.out.println("GUIFileManagementController: ProvenanceModel " + mainProvenanceViewCotroller.getProvenanceEditorController()
                                                                                                            .getProvenanceModel());
        }
        mainProvenanceViewCotroller.getGraphDrawingController()
                                   .drawGraph(mainProvenanceViewCotroller.getProvenanceEditorController()
                                                                         .getProvenanceModel()
                                                                         .getProvenanceGraph());
    }

    private JSONObject createJsonObject(String fileName) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        File file = new File(fileName);
        Object graph = parser.parse(new FileReader(file));
        return (JSONObject) graph;
    }

}
