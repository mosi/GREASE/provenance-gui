package org.grease.proveance.gui.controller;

import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.layout.mxCompactTreeLayout;
import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import org.grease.provenance.editor.ProvenanceEditorController;
import org.grease.provenance.model.ProvenanceModel;
import org.grease.provenance.model.graph.ProvenanceDependency;
import org.grease.provenance.model.graph.ProvenanceGraph;
import org.grease.provenance.model.graph.ProvenanceNode;
import org.grease.provenance.model.graph.ProvenanceNodeType;


import javax.swing.*;
import java.util.HashMap;

public class GraphDrawingController {

    private final ProvenanceModel provenanceModel;
    private final ProvenanceEditorController controller;
    private mxGraph graphDrawCanvas;
    private mxGraphComponent graphPanel;

    private final HashMap<String, mxCell> cellHashMap;


    public GraphDrawingController(ProvenanceModel provenanceModel, ProvenanceEditorController controller) {
        this.provenanceModel = provenanceModel;
        this.controller = controller;
        this.cellHashMap = new HashMap<>();
    }

    //------------------------------Draw Elements:----------------------------------------------------------------------

    //revalidate and repaint the graphPanel
    public void updateDrawing() {
        this.graphPanel.revalidate();
        this.graphPanel.repaint();
    }

    //Takes whole Provenancenode and paints/adds it onto the graph panel
    public void drawProvenanceNode(ProvenanceNode node) {
        this.drawVertex(node.getPositionX(), node.getPositionY(), node.getProvenanceNodeType(), node.getName(), node.getId());
    }

    //Takes whole Provenancedependency and paints/adds it onto the graph panel
    public void drawProvenanceDependency(ProvenanceDependency dependency) {
        String sourceId = dependency.getSourceProvenanceNode().getId();
        String targetId = dependency.getTargetProvenanceNode().getId();
        this.drawEdge(dependency.getId(), this.cellHashMap.get(sourceId), this.cellHashMap.get(targetId));
    }

    //Takes whole Provenancegraph and paints it onto the graph panel
    public void drawGraph(ProvenanceGraph graph) {
        for (ProvenanceNode node : graph.getNodes()) {
            drawProvenanceNode(node);
        }
        for (ProvenanceDependency dependency : graph.getDependencies()) {
            drawProvenanceDependency(dependency);
        }
        updateDrawing();
    }

    public void layoutGraph() {
        System.out.println("GraphDrawingController: layoutGraph");
        mxHierarchicalLayout layout = new mxHierarchicalLayout(this.graphDrawCanvas);
        layout.setParallelEdgeSpacing(15);
        layout.setOrientation(SwingConstants.WEST);
        layout.execute(this.graphDrawCanvas.getDefaultParent());
    }

    private void drawEdge(String id, mxCell start, mxCell target) {
        graphDrawCanvas.getModel().beginUpdate();
        graphDrawCanvas.insertEdge(graphDrawCanvas.getDefaultParent(), id, null, start, target);
        graphDrawCanvas.getModel().endUpdate();
    }

    private void drawVertex(double positionX, double positionY, ProvenanceNodeType provenanceNodeType, String cellValue,
                            String id) {
        String nodeTypeShape = this.getNodeTypeShape(provenanceNodeType);
        graphDrawCanvas.getModel().beginUpdate();
        mxCell cell = (mxCell) graphDrawCanvas.insertVertex(graphDrawCanvas.getDefaultParent(), id, cellValue,
                positionX, positionY, 80, 80, nodeTypeShape);
        this.cellHashMap.put(id, cell);
        graphDrawCanvas.getModel().endUpdate();
        this.updateDrawing();
    }

    //------------------------------Getters:----------------------------------------------------------------------------

    private String getNodeTypeShape(ProvenanceNodeType provenanceNodeType) {
        String elementShape = "";
        switch (provenanceNodeType) {
            case ACTIVITY:
                break;
            case ENTITY:
                elementShape = "shape=ellipse";
                break;
            case AGENT:
                elementShape = "shape=hexagon";
                break;
        }
        return elementShape;
    }

    public mxGraph getGraphDrawCanvas() {
        return graphDrawCanvas;
    }

    //------------------------------Setter:-----------------------------------------------------------------------------

    public void setGraphDrawCanvas(mxGraph graphDrawCanvas) {
        this.graphDrawCanvas = graphDrawCanvas;
    }

    public void setGraphPanel(mxGraphComponent graphPanel) {
        this.graphPanel = graphPanel;
    }

}
