package org.grease.proveance.gui.controller;

import com.mxgraph.model.mxCell;
import org.grease.proveance.gui.view.mainwindow.interaction.interactionelements.DragDropState;
import org.grease.provenance.editor.ProvenanceEditorController;
import org.grease.provenance.editor.subcontrollerEditor.GeneratingController;
import org.grease.provenance.model.ProvenanceModel;
import org.grease.provenance.model.graph.ProvenanceDependency;
import org.grease.provenance.model.graph.ProvenanceGraph;
import org.grease.provenance.model.graph.ProvenanceNode;
import org.grease.provenance.model.graph.ProvenanceNodeType;

public class GUIGeneratingController extends GeneratingController {
    MainProvenanceViewCotroller mainProvenanceViewCotroller;

    public GUIGeneratingController(ProvenanceModel provenanceModel, ProvenanceEditorController controller,
                                   MainProvenanceViewCotroller mainProvenanceViewCotroller) {
        super(provenanceModel, controller);
        this.mainProvenanceViewCotroller = mainProvenanceViewCotroller;
    }

    //----------------------------------Dependency:---------------------------------------------------------------------

    //add provenance dependency between two provenance nodes to the provenance graph and draw it onto the canvas
    @Override
    public ProvenanceDependency addDependencyBetween(String sourceNodeId, String targetNodeId) {
        ProvenanceDependency provenanceDependency = super.addDependencyBetween(sourceNodeId, targetNodeId);
        mainProvenanceViewCotroller.getGraphDrawingController().drawProvenanceDependency(provenanceDependency);
        return provenanceDependency;
    }

    //add whole provenance dependency to provenanceGraph and draw it
    @Override
    public void addDependency(ProvenanceDependency dependency) {
        super.addDependency(dependency);
        mainProvenanceViewCotroller.getGraphDrawingController().drawProvenanceDependency(dependency);
    }

    //add whole provenance node to provenanceGraph and draw it
    @Override
    public ProvenanceNode addNode(ProvenanceNode node) {
        super.addNode(node);
        mainProvenanceViewCotroller.getGraphDrawingController().drawProvenanceNode(node);
        return node;
    }

    private ProvenanceNode determineRelationshipSource(mxCell cell, ProvenanceGraph provenanceGraph) {
        ProvenanceNode provenanceNode = null;
        if (cell.isEdge()) {
            provenanceNode = mainProvenanceViewCotroller.getProvenanceEditorController()
                                                        .getProvenanceNodeById(provenanceGraph, cell.getSource()
                                                                                                    .getId());
        }
        return provenanceNode;
    }

    //----------------------------------Node:---------------------------------------------------------------------------

    private ProvenanceNode determineRelationshipTarget(mxCell cell, ProvenanceGraph provenanceGraph) {
        ProvenanceNode provenanceNode = null;
        if (cell.isEdge()) {
            provenanceNode = mainProvenanceViewCotroller.getProvenanceEditorController()
                                                        .getProvenanceNodeById(provenanceGraph, cell.getTarget()
                                                                                                    .getId());
        }
        return provenanceNode;
    }

    //add and create provenance node from dropstate (drag and drop) to provenanceGraph and draw it
    public ProvenanceNode addNodeFromState(DragDropState state) {
        ProvenanceNode node = createNodeFromState(state);
        this.addNode(node);
        return node;
    }

    private ProvenanceNode createNodeFromState(DragDropState state) {
        ProvenanceNode node = null;
        ProvenanceNodeType draggedProvenanceNodeType = state.getDragProvenanceNodeType();
        switch (draggedProvenanceNodeType) {
            case ENTITY:
                node = createEnitityNode(state.getProvenancneNodeName());
                break;
            case ACTIVITY:
                node = createActivityNode(state.getProvenancneNodeName());
                break;
            case AGENT:
                node = createAgentNode(state.getProvenancneNodeName());
                break;
        }
        node.setPositionX(state.getDropAtX());
        node.setPositionY(state.getDropAtY());
        return node;
    }

    private ProvenanceNodeType determineElementType(mxCell cell) {
        String cellType = cell.getStyle();
        ProvenanceNodeType provenanceNodeType;
        if (cellType.contains("ellipse")) {
            provenanceNodeType = ProvenanceNodeType.ENTITY;
        } else if (cellType.contains("hexagon")) {
            provenanceNodeType = ProvenanceNodeType.AGENT;
        } else {
            provenanceNodeType = ProvenanceNodeType.ACTIVITY;
        }
        return provenanceNodeType;
    }

}
