package org.grease.proveance.gui.controller;


import org.grease.proveance.gui.view.ReductionWindow.ClusterPane;
import org.grease.proveance.gui.view.ReductionWindow.ReduceView;
import org.grease.provenance.editor.ProvenanceEditorController;
import org.grease.provenance.editor.subcontrollerEditor.ReducingController;
import org.grease.provenance.model.graph.ProvenanceGraph;
import org.grease.provenance.model.graph.ProvenanceNodeType;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class ReduceViewController {
    private ProvenanceEditorController controller;
    private MainProvenanceViewCotroller mainProvenanceViewCotroller;
    private JTable clustersTable;
    private String selectedReduction;

    public ReduceViewController(ProvenanceEditorController controller,
                                MainProvenanceViewCotroller mainProvenanceViewCotroller) {
        this.mainProvenanceViewCotroller = mainProvenanceViewCotroller;
        this.controller = controller;
    }

    //paint a table of available clusters within chosen reduction id with colums for a clusters resulting node name and node-type
    public void updateClusterPane(ReduceView reduceView, String reduction) {
        ClusterPane clusterPane = reduceView.getClusterPane();

        String[][] clustersTmp = new String[controller.getProvenanceModel()
                                                      .getProvenanceGraph()
                                                      .getReductions()
                                                      .get(reduction)
                                                      .size()][3];
        int i = 0;
        for (String clusterTmp : controller.getProvenanceModel()
                                           .getProvenanceGraph()
                                           .getReductions()
                                           .get(reduction)
                                           .keySet()) {
            clustersTmp[i][0] = clusterTmp;
            clustersTmp[i][1] = controller.getProvenanceModel()
                                          .getProvenanceGraph()
                                          .getReductionClusterNameAndTypeMap()
                                          .get(reduction)
                                          .get(clusterTmp)
                                          .get("name");
            clustersTmp[i][2] = controller.getProvenanceModel()
                                          .getProvenanceGraph()
                                          .getReductionClusterNameAndTypeMap()
                                          .get(reduction)
                                          .get(clusterTmp)
                                          .get("type");
            i++;
        }

        clustersTable = new JTable(clustersTmp, new String[]{"ClusterID", "new name of reduced cluster", "Provenance Node Type"});

        Font serif = new Font("Arial", Font.BOLD, 18);
        clustersTable.setFont(serif);

        clusterPane.getViewport().setView(clustersTable);

        reduceView.revalidate();
        reduceView.repaint();
    }

    //paint a table of all available reduction ids, that can be selected
    public void loadReductions(ReduceView reduceView){
        ReducingController reducingController = controller.getReducingController();
        JTable reductionList;

        String[][] reductions = new String[controller.getProvenanceModel().getProvenanceGraph().getReductions().keySet().size()][1];
        int i = 0;
        for(String reduction : controller.getProvenanceModel().getProvenanceGraph().getReductions().keySet()){
            reductions[i][0]=reduction;
            i++;
        }

        Font serif = new Font("Arial", Font.BOLD, 18);
        reductionList = new JTable(reductions, new String[]{"reductions"});
        reductionList.setFont(serif);
        reductionList.setPreferredSize(new Dimension(299,reductions.length*100));
        reductionList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        TableModel tableModel = reductionList.getModel();

        ListSelectionModel listSelectionModel = reductionList.getSelectionModel();

        listSelectionModel.addListSelectionListener(e -> {
            this.selectedReduction = (String) tableModel.getValueAt(reductionList.getSelectedRow(), 0);
            this.updateClusterPane(mainProvenanceViewCotroller.getReduceView(), selectedReduction);
        });

        reduceView.getReductionsPane().getViewport().setView(reductionList);

    }

    //perform chosen reduction using data from clusterPane
    public ProvenanceGraph handleExecuteReductionButton(TextArea messagesTextField){
        Map<String,String> clusterNameMap = new HashMap<>();
        Map<String, ProvenanceNodeType> clusterNodeTypeMap= new HashMap<>();

        TableModel clustersTableModel = clustersTable.getModel();

        for(int i = 0; i< clustersTableModel.getRowCount();i++){
            String clusterId = (String) clustersTableModel.getValueAt(i,0);
            String clusterTargetName = (String) clustersTableModel.getValueAt(i,1);
            String clusterTargetNodeTypeString = (String) clustersTableModel.getValueAt(i,2);

            if(clusterTargetName == null || clusterTargetNodeTypeString == null ){
                messagesTextField.setText("Please specify all target names and node types before executing.");
                return null;
            }

            ProvenanceNodeType clusterTargetNodeType = stringToNodeType(clusterTargetNodeTypeString,messagesTextField);

            controller.getProvenanceModel().getProvenanceGraph().setNameAndTypeOfClusterAtReduction(selectedReduction,clusterId,clusterTargetName,clusterTargetNodeTypeString);
            clusterNameMap.put(clusterId,clusterTargetName);
            clusterNodeTypeMap.put(clusterId,clusterTargetNodeType);
        }

        return controller.getReducingController().reduceProvenanceGraph(selectedReduction,clusterNameMap,clusterNodeTypeMap);
    }

    private ProvenanceNodeType stringToNodeType(String clusterTargetNodeTypeString, TextArea messagesTextField) {

        switch (clusterTargetNodeTypeString){
            case "ENTITY":
                return ProvenanceNodeType.ENTITY;
            case "ACTIVITY":
                return ProvenanceNodeType.ACTIVITY;
            case "AGENT" :
                return ProvenanceNodeType.AGENT;
            default:
                messagesTextField.setText(clusterTargetNodeTypeString + " is not a Type of Provenance Node.");
                return null;
        }
    }

    //------------------------------------------Getter:-----------------------------------------------------------------

    public String getSelectedReduction() {
        return selectedReduction;
    }
}
