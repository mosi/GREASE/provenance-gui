package org.grease.proveance.gui.controller;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import org.neo4j.graphdb.Result;

import javax.swing.*;
import java.util.Map;

public class QueryController {
    MainProvenanceViewCotroller mainProvenanceViewCotroller;

    public QueryController(MainProvenanceViewCotroller mainProvenanceViewCotroller) {
        this.mainProvenanceViewCotroller = mainProvenanceViewCotroller;
    }

    //Performes query from text field in neo4j Database and displays result in result text field. Highlights results in displayed graph
    public void handleExecuteQueryButton(JTextArea cypherTextArea) {
        String query = cypherTextArea.getText();
        resetHighlighting();
        Result result = mainProvenanceViewCotroller.getProvenanceEditorController()
                                                   .getDatabaseController()
                                                   .performQuery(query);
        JTextArea resultLogArea = mainProvenanceViewCotroller.getMainView()
                                                             .getQueryView()
                                                             .getQueryResultLogView()
                                                             .getResultLogArea();
        String resultString = mainProvenanceViewCotroller.getProvenanceEditorController()
                                                         .getDatabaseController()
                                                         .resultToString(result);
        resultLogArea.setText(resultString);

        highlightQueryResults(result);
    }

    private void highlightQueryResults(Result result) {

        while (result.hasNext()) {
            Map<String, Object> row = result.next();
            for (String key : result.columns()) {
                String areaResult = (key + " = " + row.get(key) + "  ");
                String[] split = key.split("\\.");
                if(split.length>1) {
                    String attribute = split[1];
                    if (attribute.equals("id") || attribute.equals("relationId")) {
                        mxGraph graph = mainProvenanceViewCotroller.getGraphDrawingController().getGraphDrawCanvas();
                        graph.selectAll();
                        Object[] selectionCells = graph.getSelectionCells();
                        for (Object cells : selectionCells) {
                            if (((mxCell) cells).getId().equals(row.get(key))) {
                                mxIGraphModel model = graph.getModel();
                                model.beginUpdate();
                                if (((mxCell) cells).isVertex()) {
                                    graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, "orange", new Object[]{cells});
                                } else {
                                    graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, "orange", new Object[]{cells});
                                }

                                graph.getSelectionModel().clear();
                                model.endUpdate();
                            }
                        }
                    }
                }
            }
        }



    }

    private void resetHighlighting() {
        mxGraph graph = mainProvenanceViewCotroller.getGraphDrawingController().getGraphDrawCanvas();
        graph.selectAll();
        Object[] selectionCells = graph.getSelectionCells();
        for (Object cells : selectionCells) {
            mxIGraphModel model = graph.getModel();
            model.beginUpdate();
            graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, "#C3D9FF", new Object[]{cells});
            graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, "#6482B9", new Object[]{cells});
            graph.getSelectionModel().clear();
            model.endUpdate();
        }
    }
}
