package org.grease.proveance.gui.controller;

import org.grease.proveance.gui.view.ReductionWindow.ReduceView;
import org.grease.proveance.gui.view.mainwindow.ProvenanceEditorView;
import org.grease.proveance.gui.view.mainwindow.interaction.interactionelements.ConnectionState;
import org.grease.proveance.gui.view.mainwindow.interaction.interactionelements.DragDropState;
import org.grease.proveance.gui.view.mainwindow.provenance.ProvenanceModelView;
import org.grease.provenance.editor.ProvenanceEditorController;

/**
 * Created by Marcus on 29.10.2017.
 */
public class MainProvenanceViewCotroller {
    protected ProvenanceEditorView provenanceEditorView;
    private ProvenanceEditorController provenanceEditorController;
    private InformationViewController informationViewController;
    private GraphDrawingController graphDrawingController;
    private ReduceViewController reduceViewController;
    private GUIGeneratingController guiGeneratingController;
    private QueryController queryController;
    private GUIFileManagementController guiFileManagemantController;
    public static boolean USE_AS_VIEW_ONLY = false;

    private ReduceView reduceView;

    public MainProvenanceViewCotroller() {
        this.provenanceEditorController = new ProvenanceEditorController();
        this.informationViewController = new InformationViewController(provenanceEditorController.getProvenanceModel(), this);
        this.graphDrawingController = new GraphDrawingController(provenanceEditorController.getProvenanceModel(), provenanceEditorController);
        this.reduceViewController = new ReduceViewController(provenanceEditorController, this);
        this.guiGeneratingController = new GUIGeneratingController(provenanceEditorController.getProvenanceModel(), provenanceEditorController, this);
        this.provenanceEditorView = new ProvenanceEditorView(this);
        this.reduceView = new ReduceView(this);
        this.queryController = new QueryController(this);
        this.guiFileManagemantController = new GUIFileManagementController(this);
    }

    //dismiss current ProvenanceModel and create new one
    public void createNewProvenanceModel() {
        provenanceEditorController.createNewProvenanceModel();
        this.informationViewController = new InformationViewController(provenanceEditorController.getProvenanceModel(), this);
        this.graphDrawingController = new GraphDrawingController(provenanceEditorController.getProvenanceModel(), provenanceEditorController);

        this.provenanceEditorView.dispose();

        this.provenanceEditorView = new ProvenanceEditorView(this);
        this.reduceView = new ReduceView(this);
        this.provenanceEditorView.run();
    }

    public static void setUseAsViewOnly(boolean useAsViewOnly) {
        USE_AS_VIEW_ONLY = useAsViewOnly;
    }

    //launch GUI
    public void showView() {
        this.provenanceEditorView.run();
    }

    //--------------------------------------Handler:--------------------------------------------------------------------

    //launches reduce org.grease.proveance.gui.view
    public void handleReduceButton() {
        this.reduceView.run();
    }

    //handles drop states (when a node is dragged onto the canvas)
    public void handleDropState() {
        ProvenanceModelView provenanceModelView = (ProvenanceModelView) provenanceEditorView.getProvenanceModelTabbedView()
                                                                                            .getSelectedComponent();
        DragDropState state = DragDropState.getInstance();

        if (!provenanceModelView.getDisplayedGraphIsReduction()) {
            this.guiGeneratingController.addNodeFromState(state);
        }

    }

    //creates dependency between two provenance nodes
    public void handleConnectionState() {
        ConnectionState state = ConnectionState.getInstance();
        this.guiGeneratingController.addDependencyBetween(state.getSourceId(), state.getTargetId());
    }

    //--------------------------------------Getter:---------------------------------------------------------------------

    public ProvenanceEditorController getProvenanceEditorController() {
        return provenanceEditorController;
    }

    public InformationViewController getInformationViewController() {
        return informationViewController;
    }

    public GraphDrawingController getGraphDrawingController() {
        return graphDrawingController;
    }

    public ReduceViewController getReduceViewController() {
        return reduceViewController;
    }

    public GUIGeneratingController getGuiGeneratingController() {
        return guiGeneratingController;
    }

    public ReduceView getReduceView() {
        return reduceView;
    }

    public ProvenanceEditorView getMainView() {
        return provenanceEditorView;
    }

    public QueryController getQueryController() {
        return queryController;
    }

    public GUIFileManagementController getGuiFileManagemantController() {
        return guiFileManagemantController;
    }
}
