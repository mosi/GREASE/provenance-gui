package org.grease.proveance.gui;

import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;

public class MainApplication {
    public static void main(String[] args) {
        MainProvenanceViewCotroller mainProvenanceViewCotroller = new MainProvenanceViewCotroller();
        mainProvenanceViewCotroller.showView();
    }
}
