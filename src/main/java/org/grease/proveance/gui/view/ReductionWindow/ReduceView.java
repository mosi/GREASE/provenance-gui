package org.grease.proveance.gui.view.ReductionWindow;

import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;
import org.grease.proveance.gui.controller.ReduceViewController;
import org.grease.provenance.editor.subcontrollerEditor.ReducingController;

import javax.swing.*;
import java.awt.*;


public class ReduceView extends JFrame{

    private ReducingController reducingController;
    private ReduceViewController reduceViewController;
    private JPanel mainPanel;
    private ReductionsPane reductionsPane;
    private BorderLayout provenanceReduceBorderLayout;
    private ClusterPane clusterPane;
    private ExecuteReductionPanel executeReductionPanel;
    private MainProvenanceViewCotroller mainProvenanceViewCotroller;

    public ReduceView(MainProvenanceViewCotroller controller) {

        this.mainProvenanceViewCotroller = controller;
        this.reducingController = controller.getProvenanceEditorController().getReducingController();
        this.reduceViewController = controller.getReduceViewController();
        this.reductionsPane = new ReductionsPane();
        this.clusterPane = new ClusterPane();
        this.executeReductionPanel = new ExecuteReductionPanel(controller);
        this.provenanceReduceBorderLayout = new BorderLayout();

        mainPanel = new JPanel();
        setup();
    }

    private void setup() {
        this.setSize(1000, 700);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setTitle("Provenance Reducer");
        this.setResizable(false);

        mainPanel.setLayout(provenanceReduceBorderLayout);

        this.reductionsPane.setBorder(BorderFactory.createTitledBorder("possible Reductions"));
        mainPanel.add(reductionsPane, BorderLayout.WEST);
        this.reductionsPane.setup(reducingController);

        this.clusterPane.setBorder(BorderFactory.createTitledBorder("Cluster within chosen Reduction"));
        mainPanel.add(clusterPane,BorderLayout.EAST);
        this.clusterPane.setup(reducingController);

        this.executeReductionPanel.setBorder(BorderFactory.createTitledBorder("execute Reduction"));
        mainPanel.add(executeReductionPanel,BorderLayout.SOUTH);
        this.executeReductionPanel.setup(reduceViewController);

        this.add(mainPanel);

        this.setLocationRelativeTo(null);

    }

    public void run() {
        reduceViewController.loadReductions(this);
        this.setVisible(true);
    }

    public ClusterPane getClusterPane() {
        return clusterPane;
    }

    public ReductionsPane getReductionsPane() {
        return reductionsPane;
    }

}
