package org.grease.proveance.gui.view.ReductionWindow;


import org.grease.provenance.editor.subcontrollerEditor.ReducingController;

import javax.swing.*;
import java.awt.*;

public class ClusterPane extends JScrollPane {

    private ReducingController reducingController;

    public void setup(ReducingController reducingController){
        this.reducingController = reducingController;
        this.setPreferredSize(new Dimension(600,500));
    }

}
