package org.grease.proveance.gui.view.mainwindow.query;

import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;

import javax.swing.*;

/**
 * Created by Marcus on 08.02.2018.
 */
public class QueryInputView extends JPanel {
    private final JTextArea cypherTextArea;
    private final MainProvenanceViewCotroller controller;
    private final JButton executeButton;

    public QueryInputView(MainProvenanceViewCotroller controller) {
        this.controller = controller;
        this.cypherTextArea = new JTextArea(15, 15);
        this.executeButton = new JButton("Execute Query");

        setup();
    }

    private void setup() {
        this.setBorder(BorderFactory.createTitledBorder("Query - Input"));
        JScrollPane scrollPane = new JScrollPane(cypherTextArea);
        cypherTextArea.setFont(cypherTextArea.getFont().deriveFont(18f));
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(scrollPane);

        executeButton.addActionListener(e -> {
                    controller.getQueryController().handleExecuteQueryButton(cypherTextArea);
                }
        );
        this.add(executeButton);


    }

    public JTextArea getCypherTextArea() {
        return cypherTextArea;
    }
}
