package org.grease.proveance.gui.view.mainwindow.interaction.interactionelements;

import com.mxgraph.model.mxCell;

public class ConnectionState {
    private static ConnectionState single_instance = null;

    private String sourceId;
    private String targetId;
    private mxCell cell;

    private ConnectionState() {
    }

    public void setCell(mxCell cell) {
        this.cell = cell;
        this.sourceId = cell.getSource().getId();
        this.targetId = cell.getTarget().getId();
    }

    public static ConnectionState getInstance() {
        if (single_instance == null)
            single_instance = new ConnectionState();
        return single_instance;
    }

    public String getSourceId() {
        return sourceId;
    }

    public String getTargetId() {
        return targetId;
    }

    public mxCell getCell() {
        return cell;
    }

}
