package org.grease.proveance.gui.view.mainwindow.query;

import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;

import javax.swing.*;

/**
 * Created by Marcus on 19.02.2018.
 */
public class QueryView extends JPanel {

    private MainProvenanceViewCotroller controller;
    private QueryInputView queryInputView;
    private QueryResultLogView queryResultLogView;

    public QueryView(MainProvenanceViewCotroller controller) {
        this.controller = controller;
        queryInputView = new QueryInputView(controller);
        queryResultLogView = new QueryResultLogView();


        setup();
    }

    public QueryInputView getQueryInputView() {
        return queryInputView;
    }

    public QueryResultLogView getQueryResultLogView() {
        return queryResultLogView;
    }

    private void setup() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBorder(BorderFactory.createTitledBorder("Query"));
        this.add(queryInputView);
        this.add(queryResultLogView);
    }
}
