package org.grease.proveance.gui.view.mainwindow.interaction.interactionelements;


import org.grease.provenance.model.graph.ProvenanceNodeType;

import java.awt.*;

public class DragDropState {
    private static DragDropState single_instance = null;

    private ProvenanceNodeType dragProvenanceNodeType;
    private double dropAtX;
    private double dropAtY;
    private String provenancneNodeName;

    private DragDropState() {
    }

    // static method to create instance of Singleton class
    public static DragDropState getInstance() {
        if (single_instance == null)
            single_instance = new DragDropState();

        return single_instance;
    }

    public String getProvenancneNodeName() {
        return provenancneNodeName;
    }

    public void setProvenancneNodeName(String provenancneNodeName) {
        this.provenancneNodeName = provenancneNodeName;
    }

    public void setDropPoint(Point point) {
        this.setDropXY(point.getX(), point.getY());
    }

    public void setDropXY(double x, double y) {
        this.setDropAtX(x);
        this.setDropAtY(y);
    }

    public double getDropAtX() {
        return dropAtX;
    }

    public void setDropAtX(double dropAtX) {
        this.dropAtX = dropAtX;
    }

    public double getDropAtY() {
        return dropAtY;
    }

    public void setDropAtY(double dropAtY) {
        this.dropAtY = dropAtY;
    }

    public ProvenanceNodeType getDragProvenanceNodeType() {
        return dragProvenanceNodeType;
    }

    public void setDragProvenanceNodeType(ProvenanceNodeType dragProvenanceNodeType) {
        this.dragProvenanceNodeType = dragProvenanceNodeType;
    }
}
