package org.grease.proveance.gui.view.mainwindow.query;

import javax.swing.*;

/**
 * Created by Marcus on 19.02.2018.
 */
public class QueryResultLogView extends JPanel {

    private JTextArea resultLogArea;

    public QueryResultLogView() {
        this.resultLogArea = new JTextArea(15, 15);
        setup();
    }


    private void setup() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        JScrollPane scrollPane = new JScrollPane(resultLogArea);
        resultLogArea.setFont(resultLogArea.getFont().deriveFont(18f));
        this.setBorder(BorderFactory.createTitledBorder("Query - Results"));
        this.add(scrollPane);
    }

    public JTextArea getResultLogArea() {
        return resultLogArea;
    }
}
