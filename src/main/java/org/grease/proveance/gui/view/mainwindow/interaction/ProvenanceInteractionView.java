package org.grease.proveance.gui.view.mainwindow.interaction;

import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;
import org.grease.proveance.gui.view.mainwindow.information.InformationView;
import org.grease.proveance.gui.view.mainwindow.interaction.interactionelements.ProvenanceElementView;
import org.grease.proveance.gui.view.mainwindow.query.QueryInputView;
import org.grease.provenance.editor.ProvenanceEditorController;

import javax.swing.*;

/**
 * Created by Marcus on 08.02.2018.
 */
public class ProvenanceInteractionView extends JPanel {
    private ProvenanceElementView provenanceElementView;
    private QueryInputView queryInputView;
    private JButton executeQueryButton;
    private MainProvenanceViewCotroller controller;
    private InformationView informationView;
    private JButton reduceButton;
    private JButton layoutButton;

    public ProvenanceInteractionView(MainProvenanceViewCotroller mainProvenanceViewCotroller) {
        ProvenanceEditorController controller = mainProvenanceViewCotroller.getProvenanceEditorController();
        this.controller = mainProvenanceViewCotroller;
        this.informationView = new InformationView(mainProvenanceViewCotroller);
        this.provenanceElementView = new ProvenanceElementView(controller);
        this.queryInputView = new QueryInputView(mainProvenanceViewCotroller);
        this.executeQueryButton = new JButton("Execute Query");
        this.reduceButton = new JButton("Reduce Graph");
        this.layoutButton = new JButton("Layout Graph");

        setup();
    }

    public InformationView getInformationView() {
        return informationView;
    }

    private void setup() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(provenanceElementView);
        this.add(reduceButton);
        this.add(layoutButton);
        this.add(informationView);

        this.reduceButton.addActionListener(e -> controller.handleReduceButton());

        this.layoutButton.addActionListener(e -> controller.getGraphDrawingController().layoutGraph());
    }
}
