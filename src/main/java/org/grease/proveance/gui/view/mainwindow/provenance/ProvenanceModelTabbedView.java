package org.grease.proveance.gui.view.mainwindow.provenance;

import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;

import javax.swing.*;

public class ProvenanceModelTabbedView extends JTabbedPane {
    private final MainProvenanceViewCotroller mainProvenanceViewCotroller;

    public ProvenanceModelTabbedView(MainProvenanceViewCotroller mainProvenanceViewCotroller) {

        this.mainProvenanceViewCotroller = mainProvenanceViewCotroller;
        setup();

    }

    private void setup() {
        ProvenanceModelView provenanceModelView = new ProvenanceModelView(mainProvenanceViewCotroller);
        this.addTab("",provenanceModelView);
    }

    public ProvenanceModelView addNewProvenanceModelTab(String tabName){
        ProvenanceModelView provenanceModelView = new ProvenanceModelView(mainProvenanceViewCotroller);
        this.addTab(tabName,provenanceModelView);
        return provenanceModelView;
    }

}
