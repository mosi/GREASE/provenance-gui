package org.grease.proveance.gui.view.mainwindow.information.graphelements;

import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * Created by Marcus on 14.12.2017.
 */
public class NodeView extends JPanel {
    private static final int TEXT_LENGHT = 30;
    private final JLabel idLabel;
    private final JTextField idTextfield;
    private final JLabel typeLabel;
    private final JTextField typeTextField;
    private final JLabel descriptionLabel;
    private final JTextArea descriptionTextArea;
    private final JLabel relationshipLabel;
    private final JTextField relationshipTextField;
    private final JLabel reductionLabel;
    private final NodeViewReductions nodeViewReductionsPanel;

    private final MainProvenanceViewCotroller controller;

    public NodeView(MainProvenanceViewCotroller controller) {
        this.idLabel = new JLabel("ID: ");
        this.idTextfield = new JTextField(TEXT_LENGHT);
        this.typeLabel = new JLabel("Type: ");
        this.typeTextField = new JTextField(TEXT_LENGHT);
        this.descriptionLabel = new JLabel("Description: ");
        this.descriptionTextArea = new JTextArea(20, 10);
        this.relationshipLabel = new JLabel("Relationships:");
        this.relationshipTextField = new JTextField(TEXT_LENGHT);
        this.reductionLabel = new JLabel("Reductions:");
        this.nodeViewReductionsPanel = new NodeViewReductions();

        this.controller = controller;
        setup();
    }

    //setup NodeView panel and Listener to save changes in description
    private void setup() {
        this.setVisible(false);
        this.setLayout(new GridBagLayout());

        Font serif = new Font("Arial", Font.BOLD, 18);

        idLabel.setFont(serif);
        typeLabel.setFont(serif);
        descriptionLabel.setFont(serif);
        relationshipLabel.setFont(serif);
        reductionLabel.setFont(serif);

        idTextfield.setEditable(false);
        typeTextField.setEditable(false);
        relationshipTextField.setEditable(false);
        descriptionTextArea.setFont(serif);

        descriptionTextArea.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

            }

            @Override
            public void focusLost(FocusEvent e) {
                controller.getInformationViewController().handleDescriptionChanges();
            }
        });

        JScrollPane descriptionScrollPane = new JScrollPane(descriptionTextArea);
        descriptionScrollPane.setPreferredSize(new Dimension(360, 300));
        this.add(idLabel, fillElementIntoGridLayout(0, 0));
        this.add(idTextfield, fillElementIntoGridLayout(1, 0));
        this.add(typeLabel, fillElementIntoGridLayout(0, 1));
        this.add(typeTextField, fillElementIntoGridLayout(1, 1));
        this.add(relationshipLabel, fillElementIntoGridLayout(0, 2));
        this.add(relationshipTextField, fillElementIntoGridLayout(1, 2));
        this.add(descriptionLabel, fillElementIntoGridLayout(0, 3));
        this.add(descriptionScrollPane, fillElementIntoGridLayout(1, 3));
        this.add(reductionLabel, fillElementIntoGridLayout(0, 4));
        JScrollPane reduceScrollPane = new JScrollPane(nodeViewReductionsPanel);
        reduceScrollPane.setPreferredSize(new Dimension(365, 200));
        this.add(reduceScrollPane, fillElementIntoGridLayout(1, 4));


    }

    private GridBagConstraints fillElementIntoGridLayout(int column, int row) {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridx = column;
        gridBagConstraints.gridy = row;

        return gridBagConstraints;
    }


    //----------------------------getter and setter:--------------------------------------------------------------------


    public NodeViewReductions getNodeViewReductionsPanel() {
        return nodeViewReductionsPanel;
    }

    public JTextField getRelationshipTextField() {
        return relationshipTextField;
    }

    public JTextField getTypeTextField() {
        return typeTextField;
    }

    public JTextArea getDescriptionTextArea() {
        return descriptionTextArea;
    }

    public JTextField getIdTextfield() {
        return idTextfield;
    }

    public String getNodeId() {
        return this.idTextfield.getText();
    }
}
