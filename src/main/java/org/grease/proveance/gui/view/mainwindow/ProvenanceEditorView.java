package org.grease.proveance.gui.view.mainwindow;

import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;
import org.grease.proveance.gui.view.mainwindow.information.InformationView;
import org.grease.proveance.gui.view.mainwindow.interaction.ProvenanceInteractionView;
import org.grease.proveance.gui.view.mainwindow.menu.ProvenanceEditorMenuBar;
import org.grease.proveance.gui.view.mainwindow.provenance.ProvenanceModelTabbedView;
import org.grease.proveance.gui.view.mainwindow.provenance.ProvenanceModelView;
import org.grease.proveance.gui.view.mainwindow.query.QueryView;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Marcus on 29.10.2017.
 */
public class ProvenanceEditorView extends JFrame {
    private MainProvenanceViewCotroller mainProvenanceViewCotroller;

    //related Gui elements:
    private ProvenanceEditorMenuBar provenanceEditorMenuBar;
    private JPanel mainPanel;
    private ProvenanceModelView provenanceModelView;
    private BorderLayout provenanceEditorBorderLayout;
    private ProvenanceInteractionView provenanceInteractionView;
    private ProvenanceModelTabbedView provenanceModelTabbedView;

    private QueryView queryView;

    public ProvenanceEditorView(MainProvenanceViewCotroller mainProvenanceViewCotroller) {

        this.mainProvenanceViewCotroller = mainProvenanceViewCotroller;
        provenanceModelView = new ProvenanceModelView(mainProvenanceViewCotroller);
        provenanceModelTabbedView = new ProvenanceModelTabbedView(mainProvenanceViewCotroller);
        mainPanel = new JPanel();
        provenanceEditorMenuBar = new ProvenanceEditorMenuBar(mainProvenanceViewCotroller);
        provenanceEditorBorderLayout = new BorderLayout();
        provenanceInteractionView = new ProvenanceInteractionView(mainProvenanceViewCotroller);
        queryView = new QueryView(mainProvenanceViewCotroller);
        setup();
    }

    private void setup() {
        this.setSize(1920, 900);
        if(MainProvenanceViewCotroller.USE_AS_VIEW_ONLY){
            this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        }else {
            this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        }
        this.setTitle("Provenance Editor");
        this.setResizable(false);
        this.setJMenuBar(provenanceEditorMenuBar);

        mainPanel.setLayout(provenanceEditorBorderLayout);
        provenanceModelView.setBorder(BorderFactory.createTitledBorder("Provenance-Editor"));
        mainPanel.add(provenanceModelTabbedView, BorderLayout.CENTER);

        mainPanel.add(queryView, BorderLayout.EAST);

        provenanceInteractionView.setBorder(BorderFactory.createTitledBorder("Elements"));
        mainPanel.add(provenanceInteractionView, BorderLayout.WEST);

        this.add(mainPanel);

        this.setLocationRelativeTo(null);

        provenanceModelTabbedView.addChangeListener(e -> {
            ProvenanceModelView provenanceModelView = (ProvenanceModelView) provenanceModelTabbedView.getSelectedComponent();
            mainProvenanceViewCotroller.getGraphDrawingController()
                                       .setGraphPanel(provenanceModelView.getGraphPanel());
            mainProvenanceViewCotroller.getGraphDrawingController()
                                       .setGraphDrawCanvas(provenanceModelView.getGraph());
        });
    }

    public void run() {
        SwingUtilities.invokeLater(this::setup);
        this.setVisible(true);
    }

    //------------------------------------------getter:-----------------------------------------------------------------

    public QueryView getQueryView() {
        return queryView;
    }

    public InformationView getInformationView() {
        return provenanceInteractionView.getInformationView();
    }

    public ProvenanceModelTabbedView getProvenanceModelTabbedView() {
        return provenanceModelTabbedView;
    }
}
