package org.grease.proveance.gui.view.mainwindow.provenance;


import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxEvent;
import com.mxgraph.view.mxGraph;
import org.grease.proveance.gui.controller.InformationViewController;
import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;
import org.grease.proveance.gui.view.mainwindow.information.InformationView;
import org.grease.proveance.gui.view.mainwindow.interaction.interactionelements.ConnectionState;
import org.grease.proveance.gui.view.mainwindow.interaction.interactionelements.ElementDropTargetListener;
import org.grease.provenance.editor.ProvenanceEditorController;
import org.grease.provenance.model.graph.ProvenanceDependency;
import org.grease.provenance.model.graph.ProvenanceGraph;
import org.grease.provenance.model.graph.ProvenanceNode;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * Created by Marcus on 02.11.2017.
 */
public class ProvenanceModelView extends JPanel {

    private final MainProvenanceViewCotroller mainProvenanceViewCotroller;

    private JPopupMenu popupMenu;
    private mxGraph graph;
    private mxGraphComponent graphPanel;
    private Boolean displayedGraphIsReduction = false;
    private ProvenanceGraph provenanceGraph;
    private Boolean debug = true;

    public ProvenanceModelView(MainProvenanceViewCotroller mainProvenanceViewCotroller) {

        this.mainProvenanceViewCotroller = mainProvenanceViewCotroller;

        addPopupMenu();
        initiateGraph(mainProvenanceViewCotroller.getProvenanceEditorController());

        TransferHandler transferHandler = createTransferHandler();

        graphPanel.setTransferHandler(transferHandler);

        if(!MainProvenanceViewCotroller.USE_AS_VIEW_ONLY){
            new ElementDropTargetListener(graphPanel, mainProvenanceViewCotroller);
        }

        setup();

    }

    private void setup() {

        graphPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        graphPanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        graphPanel.getHorizontalScrollBar().setUnitIncrement(20);
        graphPanel.getViewport().setViewSize(new Dimension(10000, 900));
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

        this.add(graphPanel);
    }

    private TransferHandler createTransferHandler() {
        return new TransferHandler() {
            @Override
            public boolean canImport(TransferSupport support) {
                return support.isDrop() && support.isDataFlavorSupported(DataFlavor.imageFlavor);

            }

            @Override
            public boolean importData(TransferSupport support) {
                if (!canImport(support)) {
                    return false;
                }

                Transferable transferable = support.getTransferable();
                Icon ico;
                try {
                    ico = (Icon) transferable.getTransferData(DataFlavor.imageFlavor);
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
                graphPanel.add(new JLabel(ico));
                return true;
            }
        };
    }

    private void addPopupMenu() {
        popupMenu = new JPopupMenu();
        JMenuItem delete = new JMenuItem("delete");
        delete.addActionListener(e -> {
            mainProvenanceViewCotroller.getProvenanceEditorController()
                                       .getDatabaseController()
                                       .deleteElements(graph.getSelectionCells());
            graph.removeCells(graph.getSelectionCells());
        });
        popupMenu.add(delete);
        this.add(popupMenu);
    }

    private void initiateGraph(ProvenanceEditorController controller) {
        graph = new mxGraph();
        graphPanel = new mxGraphComponent(graph);
        graphPanel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
        graphPanel.getConnectionHandler().addListener(mxEvent.CONNECT, (o, mxEventObject) -> {
            mxCell cell = (mxCell) mxEventObject.getProperties().get("cell");
            if (cell.isEdge()) {
                graph.getModel().remove(cell);
                if (cell.getTarget() != null) {
                    ConnectionState.getInstance().setCell(cell);
                    mainProvenanceViewCotroller.handleConnectionState();
                }
            }
        });
        graphPanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        graphPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        graphPanel.getGraphControl().addMouseListener(new GraphControlListener());

        mainProvenanceViewCotroller.getGraphDrawingController().setGraphDrawCanvas(this.graph);
        mainProvenanceViewCotroller.getGraphDrawingController().setGraphPanel(this.graphPanel);
        controller.createNewProvenanceGraph(controller.getProvenanceModel());
    }

    public ProvenanceGraph getProvenanceGraph() {
        return provenanceGraph;
    }

    //----------------------------------------getter and setter:--------------------------------------------------------

    public void setProvenanceGraph(ProvenanceGraph provenanceGraph) {
        this.provenanceGraph = provenanceGraph;
    }

    public mxGraph getGraph() {
        return graph;
    }

    public mxGraphComponent getGraphPanel() {
        return graphPanel;
    }

    public Boolean getDisplayedGraphIsReduction() {
        return displayedGraphIsReduction;
    }

    public void setDisplayedGraphReduction() {
        displayedGraphIsReduction = true;
    }

    //controls how nodes can be moved by mouse within canvas and that information is shown when a node is clicked
    public class GraphControlListener extends MouseAdapter {
        private Point origin;

        @Override
        public void mousePressed(MouseEvent e) {
            origin = new Point(e.getPoint());

        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (origin != null) {
                JViewport viewport = graphPanel.getViewport();
                if (viewport != null) {
                    int dx = origin.x - e.getX();
                    int dy = origin.y - e.getY();

                    Rectangle view = viewport.getViewRect();
                    view.x += dx;
                    view.y += dy;

                    graphPanel.scrollRectToVisible(view);
                }
            }

        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isRightMouseButton(e)) {
                popupMenu.show(graphPanel, e.getX(), e.getY());
            }
            if (SwingUtilities.isLeftMouseButton(e)) {
                mxCell cell = (mxCell) graphPanel.getCellAt(e.getX(), e.getY());
                if (cell != null) {
                    String cellId = cell.getId();
                    System.out.println(cell.getId());
                    InformationView informationView = mainProvenanceViewCotroller.getMainView().getInformationView();
                    ProvenanceGraph provenanceGraphOfModel = mainProvenanceViewCotroller.getProvenanceEditorController()
                                                                                        .getProvenanceModel()
                                                                                        .getProvenanceGraph();
                    InformationViewController informationViewController = mainProvenanceViewCotroller.getInformationViewController();
                    if (cell.isVertex()) {
                        ProvenanceNode provenanceNode;
                        ProvenanceModelView provenanceModelView = (ProvenanceModelView) mainProvenanceViewCotroller.getMainView()
                                                                                                                   .getProvenanceModelTabbedView()
                                                                                                                   .getSelectedComponent();
                        if (!provenanceModelView.getDisplayedGraphIsReduction()) {
                            provenanceNode = mainProvenanceViewCotroller.getProvenanceEditorController()
                                                                        .getProvenanceNodeById(provenanceGraphOfModel, cellId);
                        } else {
                            provenanceNode = mainProvenanceViewCotroller.getProvenanceEditorController()
                                                                        .getProvenanceNodeById(provenanceGraph, cellId);
                        }

                        informationViewController.updateNodeInformationView(informationView, provenanceNode);
                    }
                    if (cell.isEdge()) {
                        ProvenanceDependency provenanceDependency = mainProvenanceViewCotroller.getProvenanceEditorController()
                                                                                               .getProvenanceRelationshipById(provenanceGraphOfModel, cellId);
                        informationViewController.updateEdgeInformationView(informationView, provenanceDependency);
                    }
                }


            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            for (Object cell : graph.getSelectionCells()) {
                mxCell node = (mxCell) cell;
                if (node != null) {
                    if (node.isVertex()) {
                        ProvenanceGraph graph = mainProvenanceViewCotroller.getProvenanceEditorController()
                                                                           .getProvenanceModel()
                                                                           .getProvenanceGraph();
                        ProvenanceNode provenanceNode;
                        if (!getDisplayedGraphIsReduction()) {
                            provenanceNode = mainProvenanceViewCotroller.getProvenanceEditorController()
                                                                        .getProvenanceNodeById(graph, node.getId());
                        } else {
                            provenanceNode = mainProvenanceViewCotroller.getProvenanceEditorController()
                                                                        .getProvenanceNodeById(provenanceGraph, node.getId());
                        }
                        double positionX = provenanceNode.getPositionX();
                        double positionY = provenanceNode.getPositionY();
                        provenanceNode.setPositionX(node.getGeometry().getX());
                        provenanceNode.setPositionY(node.getGeometry().getY());
                        double positionX1 = provenanceNode.getPositionX();
                        double positionY1 = provenanceNode.getPositionY();
                        if (debug) {
                            System.out.println("ProvenanceModelView: alt x " + positionX + " neu x " + positionX1 + "alt y " + positionY + " neu y " + positionY1);
                        }
                    }
                }

            }
        }

        private Boolean containsVertex(Object[] cells) {
            for (Object cell : cells) {
                if (((mxCell) cell).isVertex()) {
                    return true;
                }
            }
            return false;
        }
    }
}

