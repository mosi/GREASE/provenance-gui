package org.grease.proveance.gui.view.ReductionWindow;


import org.grease.provenance.editor.subcontrollerEditor.ReducingController;

import javax.swing.*;
import java.awt.*;

public class ReductionsPane extends JScrollPane {
    private ReducingController reducingController;

    public void setup(ReducingController reducingController){
        this.setPreferredSize(new Dimension(350,500));
        this.reducingController = reducingController;
    }

}
