package org.grease.proveance.gui.view.mainwindow.information;

import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;
import org.grease.proveance.gui.view.mainwindow.information.graphelements.NodeView;
import org.grease.proveance.gui.view.mainwindow.information.graphelements.RelationshipView;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Marcus on 29.10.2017.
 */
public class InformationView extends JPanel {

    private final MainProvenanceViewCotroller controller;
    private final NodeView nodeView;
    private final RelationshipView relationshipView;

    public InformationView(MainProvenanceViewCotroller controller) {

        this.controller = controller;
        this.nodeView = new NodeView(controller);
        this.relationshipView = new RelationshipView(controller);
        setup();
    }

    private void setup() {
        this.setBorder(BorderFactory.createTitledBorder("Element Information"));
        this.setLayout(new BorderLayout());

        this.add(nodeView, BorderLayout.WEST);
        this.add(relationshipView, BorderLayout.WEST);

    }

    public NodeView getNodeView() {
        return nodeView;
    }

    public RelationshipView getRelationshipView() {
        return relationshipView;
    }

}
