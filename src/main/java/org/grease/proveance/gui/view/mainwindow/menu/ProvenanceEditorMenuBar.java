package org.grease.proveance.gui.view.mainwindow.menu;


import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;
import org.grease.proveance.gui.view.mainwindow.provenance.ProvenanceModelView;

import javax.swing.*;
import java.io.File;


/**
 * Created by Marcus on 29.10.2017.
 */
public class ProvenanceEditorMenuBar extends JMenuBar {

    private final JMenu projectMenu;
    private final JMenuItem newModel;
    private final JMenuItem saveModel;
    private final JMenuItem loadModel;
    private final JMenuItem toDot;
    private final MainProvenanceViewCotroller mainProvenanceViewCotroller;


    public ProvenanceEditorMenuBar(MainProvenanceViewCotroller mainProvenanceViewCotroller) {

        this.mainProvenanceViewCotroller = mainProvenanceViewCotroller;

        projectMenu = new JMenu("Project");
        newModel = new JMenuItem("New Model");
        saveModel = new JMenuItem("Save Model");
        loadModel = new JMenuItem("Load Model");
        toDot = new JMenuItem("Save Dot-File");

        saveModel.addActionListener(e -> {
            try {
                mainProvenanceViewCotroller.getGuiFileManagemantController().saveGraphToFile();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });

        this.loadModel.addActionListener(e -> {
            try {
                mainProvenanceViewCotroller.getGuiFileManagemantController().loadGraphFromFile();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });

        this.toDot.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            int result = fileChooser.showOpenDialog(mainProvenanceViewCotroller.getMainView());
            if (result == JFileChooser.APPROVE_OPTION) {
                String path = fileChooser.getSelectedFile().getPath();
                System.out.println("Choose path to save dot: " + path);
                File dotFile = new File(path + ".dot");

                ProvenanceModelView provenanceModelView = (ProvenanceModelView) mainProvenanceViewCotroller.getMainView()
                                                                                                           .getProvenanceModelTabbedView()
                                                                                                           .getSelectedComponent();

                mainProvenanceViewCotroller.getProvenanceEditorController()
                                           .getFileManagementController()
                                           .saveToDot(provenanceModelView.getProvenanceGraph(), dotFile);

            }
        });

        setup();
    }


    private void setup() {
        projectMenu.add(newModel);
        projectMenu.add(saveModel);
        projectMenu.add(loadModel);
        projectMenu.addSeparator();
        projectMenu.add(toDot);
        this.add(projectMenu);

    }

}
