package org.grease.proveance.gui.view.ReductionWindow;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;
import org.grease.proveance.gui.controller.ReduceViewController;
import org.grease.proveance.gui.view.mainwindow.provenance.ProvenanceModelView;
import org.grease.provenance.model.graph.ProvenanceGraph;

import javax.swing.*;
import java.awt.*;

public class ExecuteReductionPanel extends JPanel {
    private ReduceViewController reduceViewController;
    private JButton reduceButton;
    private TextArea messagesTextField;
    private MainProvenanceViewCotroller mainProvenanceViewCotroller;

    public ExecuteReductionPanel(MainProvenanceViewCotroller mainProvenanceViewCotroller) {
        this.mainProvenanceViewCotroller = mainProvenanceViewCotroller;
        this.setPreferredSize(new Dimension(990, 70));
        this.messagesTextField = new TextArea();
        this.messagesTextField.setPreferredSize(new Dimension(700, 60));
        this.reduceButton = new JButton("execute Reduction");
    }

    void setup(ReduceViewController reduceViewController) {
        this.setLayout(new BorderLayout());
        this.reduceViewController = reduceViewController;
        this.add(messagesTextField, BorderLayout.WEST);
        this.add(reduceButton, BorderLayout.EAST);

        this.reduceButton.addActionListener(e -> {
            ProvenanceGraph reducedGraph = reduceViewController.handleExecuteReductionButton(messagesTextField);
            String key = mainProvenanceViewCotroller.getReduceViewController().getSelectedReduction();
            ProvenanceModelView provenanceModelView1 = mainProvenanceViewCotroller.getMainView()
                                                                                  .getProvenanceModelTabbedView()
                                                                                  .
                                                                                          addNewProvenanceModelTab("Reduction " + key);
            provenanceModelView1.setDisplayedGraphReduction();
            mxGraph mxGraph = provenanceModelView1.getGraph();
            mxGraphComponent graphPanel = provenanceModelView1.getGraphPanel();
            mainProvenanceViewCotroller.getGraphDrawingController().setGraphDrawCanvas(mxGraph);
            mainProvenanceViewCotroller.getGraphDrawingController().setGraphPanel(graphPanel);
            mainProvenanceViewCotroller.getGraphDrawingController().drawGraph(reducedGraph);
            provenanceModelView1.setProvenanceGraph(reducedGraph);
        });
    }
}
