package org.grease.proveance.gui.view.mainwindow.interaction.interactionelements;

import com.mxgraph.swing.mxGraphComponent;
import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;

/**
 * Created by Marcus on 16.11.2017.
 */
public class ElementDropTargetListener extends DropTargetAdapter {
    MainProvenanceViewCotroller controller;
    private DropTarget dropTarget;

    public ElementDropTargetListener(mxGraphComponent targetPanel, MainProvenanceViewCotroller controller) {
        this.controller = controller;
        this.dropTarget = new DropTarget(targetPanel, DnDConstants.ACTION_COPY_OR_MOVE, this, true, null);
    }

    @Override
    public void drop(DropTargetDropEvent event) {
        Component component = dropTarget.getComponent();
        Point dropPoint = component.getMousePosition();
        DragDropState.getInstance().setDropPoint(dropPoint);
        String name = JOptionPane.showInputDialog(null, "Name the Element");
        DragDropState.getInstance().setProvenancneNodeName(name);
        if (event.isDataFlavorSupported(DataFlavor.imageFlavor)) {
            controller.handleDropState();
        }
    }
}
