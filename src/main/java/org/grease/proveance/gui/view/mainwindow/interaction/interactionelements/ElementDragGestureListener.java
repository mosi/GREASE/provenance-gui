package org.grease.proveance.gui.view.mainwindow.interaction.interactionelements;


import org.grease.provenance.editor.ProvenanceEditorController;
import org.grease.provenance.model.graph.ProvenanceNodeType;

import javax.swing.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;

/**
 * Created by Marcus on 16.11.2017.
 */
public class ElementDragGestureListener implements DragGestureListener {
    private ProvenanceEditorController controller;

    public ElementDragGestureListener(ProvenanceEditorController controller) {
        this.controller = controller;
    }

    @Override
    public void dragGestureRecognized(DragGestureEvent event) {
        JLabel ProvenanceElement = (JLabel) event.getComponent();
        determineProvenanceElementType(ProvenanceElement);

        Transferable transferable = new Transferable() {
            @Override
            public DataFlavor[] getTransferDataFlavors() {
                return new DataFlavor[]{DataFlavor.imageFlavor};
            }

            @Override
            public boolean isDataFlavorSupported(DataFlavor flavor) {
                return isDataFlavorSupported(flavor);
            }

            @Override
            public Object getTransferData(DataFlavor flavor) {
                return ProvenanceElement.getText();
            }
        };
        event.startDrag(null, transferable);
    }

    private void determineProvenanceElementType(JLabel component) {
        String componentText = component.getText();
        ProvenanceNodeType type = ProvenanceNodeType.valueOf(componentText);
        DragDropState.getInstance().setDragProvenanceNodeType(type);

    }
}
