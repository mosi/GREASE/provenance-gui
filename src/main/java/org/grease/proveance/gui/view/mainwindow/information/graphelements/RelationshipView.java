package org.grease.proveance.gui.view.mainwindow.information.graphelements;

import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * Created by Marcus on 14.12.2017.
 */
public class RelationshipView extends JPanel {
    private static final int TEXT_LENGHT = 30;
    private JLabel idLabel;
    private JTextField idTextfield;
    private JLabel dependencyLabel;
    private JTextField dependencyTextField;
    private JLabel roleLabel;
    private JTextField roleTextField;
    private JLabel sourceLabel;
    private JTextField sourceTextField;
    private JLabel targetLabel;
    private JTextField targetTextField;

    private String relationshipID;

    MainProvenanceViewCotroller controller;

    public RelationshipView(MainProvenanceViewCotroller controller) {
        this.idLabel = new JLabel("ID: ");
        this.idTextfield = new JTextField(TEXT_LENGHT);
        this.dependencyLabel = new JLabel("Dependency: ");
        this.dependencyTextField = new JTextField(TEXT_LENGHT);
        this.sourceLabel = new JLabel("Source: ");
        this.sourceTextField = new JTextField(TEXT_LENGHT);
        this.targetLabel = new JLabel("Target: ");
        this.targetTextField = new JTextField(TEXT_LENGHT);
        this.roleLabel = new JLabel("Role: ");
        this.roleTextField = new JTextField(TEXT_LENGHT);
        this.controller = controller;


        setup();
    }

    //setup RelationshipView panel and Listener to save changes in role
    private void setup() {
        this.setVisible(false);
        setLabelFonts();

        dependencyTextField.setEditable(false);

        roleTextField.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

            }

            @Override
            public void focusLost(FocusEvent e) {
                controller.getInformationViewController().handleRoleChanges();
            }
        });

        GridBagLayout gridBagLayout = new GridBagLayout();
        this.setLayout(gridBagLayout);


        this.add(idLabel, fillElementIntoGridLayout(0, 0));
        this.add(idTextfield, fillElementIntoGridLayout(1, 0));
        this.add(sourceLabel, fillElementIntoGridLayout(0, 1));
        this.add(sourceTextField, fillElementIntoGridLayout(1, 1));
        this.add(targetLabel, fillElementIntoGridLayout(0, 2));
        this.add(targetTextField, fillElementIntoGridLayout(1, 2));
        this.add(roleLabel, fillElementIntoGridLayout(0, 3));
        this.add(roleTextField, fillElementIntoGridLayout(1, 3));
        this.add(dependencyLabel, fillElementIntoGridLayout(0, 4));
        this.add(dependencyTextField, fillElementIntoGridLayout(1, 4));


    }

    private GridBagConstraints fillElementIntoGridLayout(int column, int row) {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridx = column;
        gridBagConstraints.gridy = row;

        return gridBagConstraints;
    }

    //----------------------------getter and setter:--------------------------------------------------------------------

    private void setLabelFonts() {
        Font serif = new Font("Arial", Font.BOLD, 16);
        idLabel.setFont(serif);
        idTextfield.setEditable(false);
        sourceLabel.setFont(serif);
        sourceTextField.setEditable(false);
        targetLabel.setFont(serif);
        targetTextField.setEditable(false);
        dependencyLabel.setFont(serif);
        dependencyTextField.setEditable(false);
        roleLabel.setFont(serif);
    }

    public JTextField getIdTextfield() {
        return idTextfield;
    }

    public JTextField getRoleTextField() {
        return roleTextField;
    }

    public void setRoleTextField(JTextField roleTextField) {
        this.roleTextField = roleTextField;
    }

    public JTextField getDependencyTextField() {
        return dependencyTextField;
    }

    public JTextField getSourceTextField() {
        return sourceTextField;
    }

    public JTextField getTargetTextField() {
        return targetTextField;
    }

    public String getRelationshipID() {
        return relationshipID;
    }

    public void setRelationshipID(String relationshipID) {
        this.relationshipID = relationshipID;
    }
}
